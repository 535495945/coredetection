#pragma once

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#define MAXLINE 1024

class PLCClient
{
public:
	//Constructor  
	PLCClient(const char* Addr, int Port, int Id);
	//连接  
	int Connect();
	//发送TCP包  
	int SendMsg(const char* msg, int len);
	//接收TCP包
	int RecvMsg();
	//关闭  
	void Close();
	//发送modbus包  
	void Modbus_sender_single(int Ref, int addr, int value);

private:
	struct sockaddr_in sockaddr;
	int socketfd;
	int port;
	const char* address;
	int id;
};