#include "OpenCVImage.h"

OpenCVImage::OpenCVImage()
{
	allThreadStop = true;

	threadStartState = false;
	carbonBlockThreadState = false;
	clientPLCThreadState = false;
}


OpenCVImage::~OpenCVImage()
{
}

OpenCVImage* OpenCVImage::instance = new OpenCVImage();
OpenCVImage* OpenCVImage::getInstance() {
	return instance;
}

void OpenCVImage::AllThreadStop()
{

}

bool OpenCVImage::createCameraThreadStart()
{
	pthread_t threadID;

	pthread_create(&threadID, NULL, threadCameraFction, NULL);
}

bool OpenCVImage::clientPLCThreadStart()
{
	pthread_t threadID;

	pthread_create(&threadID, NULL, clientPLCThread, NULL);
}

bool OpenCVImage::carbonBlockThreadStart()
{
	pthread_t threadID;

	pthread_create(&threadID, NULL, carbonBlockThread, NULL);
}

void *OpenCVImage::threadCameraFction(void *arg)
{
	int nRet = MV_OK;

	void* handle = NULL;
	GrabCamera* grabCamera = new GrabCamera();
	do
	{
		MV_CC_DEVICE_INFO_LIST stDeviceList;
		memset(&stDeviceList, 0, sizeof(MV_CC_DEVICE_INFO_LIST));

		// 枚举设备
		// enum device
		nRet = MV_CC_EnumDevices(MV_GIGE_DEVICE | MV_USB_DEVICE, &stDeviceList);
		if (MV_OK != nRet)
		{
			printf("MV_CC_EnumDevices fail! nRet [%x]\n", nRet);
			break;
		}

		if (stDeviceList.nDeviceNum > 0)
		{
			for (int i = 0; i < stDeviceList.nDeviceNum; i++)
			{
				printf("[device %d]:\n", i);
				MV_CC_DEVICE_INFO* pDeviceInfo = stDeviceList.pDeviceInfo[i];
				if (NULL == pDeviceInfo)
				{
					break;
				}
				grabCamera->PrintDeviceInfo(pDeviceInfo);
			}
		}
		else
		{
			printf("Find No Devices!\n");
			break;
		}

		printf("Please Intput camera index: ");
		unsigned int nIndex = 0;
		scanf("%d", &nIndex);

		if (nIndex >= stDeviceList.nDeviceNum)
		{
			printf("Intput error!\n");
			break;
		}

		// 选择设备并创建句柄
		// select device and create handle
		nRet = MV_CC_CreateHandle(&handle, stDeviceList.pDeviceInfo[nIndex]);
		if (MV_OK != nRet)
		{
			printf("MV_CC_CreateHandle fail! nRet [%x]\n", nRet);
			break;
		}

		// 打开设备
		// open device
		nRet = MV_CC_OpenDevice(handle);
		if (MV_OK != nRet)
		{
			printf("MV_CC_OpenDevice fail! nRet [%x]\n", nRet);
			break;
		}

		// ch:探测网络最佳包大小(只对GigE相机有效) | en:Detection network optimal package size(It only works for the GigE camera)
		if (stDeviceList.pDeviceInfo[nIndex]->nTLayerType == MV_GIGE_DEVICE)
		{
			int nPacketSize = MV_CC_GetOptimalPacketSize(handle);
			if (nPacketSize > 0)
			{
				nRet = MV_CC_SetIntValue(handle, "GevSCPSPacketSize", nPacketSize);
				if (nRet != MV_OK)
				{
					printf("Warning: Set Packet Size fail nRet [0x%x]!\n", nRet);
				}
			}
			else
			{
				printf("Warning: Get Packet Size fail nRet [0x%x]!\n", nPacketSize);
			}
		}

		// 设置触发模式为off
		// set trigger mode as off
		nRet = MV_CC_SetEnumValue(handle, "TriggerMode", 0);
		if (MV_OK != nRet)
		{
			printf("MV_CC_SetTriggerMode fail! nRet [%x]\n", nRet);
			break;
		}

		// 开始取流
		// start grab image
		nRet = MV_CC_StartGrabbing(handle);
		if (MV_OK != nRet)
		{
			printf("MV_CC_StartGrabbing fail! nRet [%x]\n", nRet);
			break;
		}

		pthread_t nThreadID;
		nRet = pthread_create(&nThreadID, NULL, grabCamera->WorkThread, handle);
		if (nRet != 0)
		{
			printf("thread create failed.ret = %d\n", nRet);
			break;
		}

		grabCamera->PressEnterToExit();

		// 停止取流
		// end grab image
		nRet = MV_CC_StopGrabbing(handle);
		if (MV_OK != nRet)
		{
			printf("MV_CC_StopGrabbing fail! nRet [%x]\n", nRet);
			break;
		}

		// 关闭设备
		// close device
		nRet = MV_CC_CloseDevice(handle);
		if (MV_OK != nRet)
		{
			printf("MV_CC_CloseDevice fail! nRet [%x]\n", nRet);
			break;
		}

		// 销毁句柄
		// destroy handle
		nRet = MV_CC_DestroyHandle(handle);
		if (MV_OK != nRet)
		{
			printf("MV_CC_DestroyHandle fail! nRet [%x]\n", nRet);
			break;
		}
	} while (0);

	if (nRet != MV_OK)
	{
		if (handle != NULL)
		{
			MV_CC_DestroyHandle(handle);
			handle = NULL;
		}
	}

}


void *OpenCVImage::clientPLCThread(void *arg) 
{
	while (OpenCVImage::getInstance()->allThreadStop) {
		int connect_error;
		PLCClient client("192.168.2.1", 502, 1);
		printf("Waiting to connection\n");
		connect_error = client.Connect();
		if (connect_error != 0) {
			sleep(1000);
			continue;
		}
		bool end1 = true;
		//client.Modbus_sender_single(2, 1, 223);
		int carbonInfoValue = 0;
		string blockid = ""; //当前需检测碳块
		while (OpenCVImage::getInstance()->allThreadStop)
		{
			if (connect_error == 0)
			{
				unsigned char buf[12];
				buf[0] = 0;
				buf[1] = 0;
				buf[2] = 0;
				buf[3] = 0;
				buf[4] = 0;
				buf[5] = 6;
				buf[6] = 1;//从机ID  
				buf[7] = 3;//命令代码  
				buf[8] = 0;
				buf[9] = 0;
				buf[10] = 0;
				buf[11] = 1;
				if (client.SendMsg((char*)buf, 12) == 1)
				{
					break;
				}
				//client.Modbus_sender_single(2, 40001, 223);
				int res = client.RecvMsg();

				//接收失败，尝试重新建立连接
				SqlUtil *sqlUtil = SqlUtil::getInstance();
				if (res == 0 && carbonInfoValue == 0)		//碳块检测尚未开始
				{
					sqlUtil->carbonBlockStart = false;
					sqlUtil->carbonBlockEnd = false;
				}
				else if (res == 0 && carbonInfoValue == 1) {	//碳块检测已经开始，并且已经结束一个碳块，下一个尚未开始
					sqlUtil->carbonBlockStart = false;
					sqlUtil->carbonBlockEnd = true;
				}
				else if (res == 1 && carbonInfoValue == 0) {	//碳块检测刚刚开始，上一个碳块已经结束，下一个碳块刚开始
					sqlUtil->carbonBlockStart = true;
					sqlUtil->carbonBlockEnd = false;
					//生成碳块信息
					SqlUtil *sqlUtil = SqlUtil::getInstance();
					time_t timep;
					time(&timep);
					char start[64];
					strftime(start, sizeof(start), "%Y/%m/%d/%H/%M/%S", localtime(&timep));
					char carbonBlockId[256];
					//查询当天谈款数量
					//XY_生产线编号_日期_碳块序号
					snprintf(carbonBlockId, 256, "%s_%s_%s_%d", "XY", "1", start, sqlUtil->getSamedayBlockNum() + 1);
					sqlUtil->createBlockInfo(1, carbonBlockId, "1");

				}
				else if (res == 1 && carbonInfoValue == 1) {	//碳块检测已经开始，并且正在检测其中一个碳块
					sqlUtil->carbonBlockStart = true;
					sqlUtil->carbonBlockEnd = true;
				}
				carbonInfoValue = res;
				sleep(100);
				{
					if (blockid.empty())
					{
						blockid = sqlUtil->getBlockNum();
					}

					if (sqlUtil->getspurtcode(blockid.c_str()) && res == 0)
					{
						//调用喷码
						unsigned char buf[12];
						buf[0] = 0;
						buf[1] = 0;
						buf[2] = 0;
						buf[3] = 0;
						buf[4] = 0;//寄存器
						buf[5] = 6;
						buf[6] = 1;//从机ID  
						buf[7] = 6;//命令代码  
						buf[8] = 0;
						buf[9] = 1;
						buf[10] = 0;
						buf[11] = 1;
						if (client.SendMsg((char*)buf, 12) == 0)
						{
							sqlUtil->setspurtcode(blockid.c_str());
							blockid = "";
							int res = client.RecvMsg();
						}
					}
					else  if (res == 0) {
						blockid = "";
					}
				}
			}
		}

		client.Close();
	}
}

void *OpenCVImage::carbonBlockThread(void *arg) 
{

}