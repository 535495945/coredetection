#include "SqlUtil.h"


SqlUtil::SqlUtil()
{
	char tmp[400];
	//database configuartion
	char dbuser[30] = "root";
	char dbpasswd[30] = "123456789"; // it must be    changed
									 //char dbpasswd[30] = "111111"; // it must be    changed
	char dbip[30] = "localhost";
	char dbname[50] = "carbonblock";
	char tablename[50] = "sys_user_info";
	char *query = NULL;

	int rt;//return value  

	con = mysql_init((MYSQL*)0);

	if (con != NULL && mysql_real_connect(con, dbip, dbuser, dbpasswd, dbname, 3306, NULL, 0)) {
		if (!mysql_select_db(con, dbname)) {
			printf("Select successfully the database!\n");
			con->reconnect = 1;
			query = "set names \'GBK\'";
			rt = mysql_real_query(con, query, strlen(query));
			if (rt) {
				printf("Error making query: %s !!!\n", mysql_error(con));
			}
			else {
				printf("query %s succeed!\n", query);
			}
		}
	}
	else {
		
	}

	//init CrackDetect
	carbonBlockStart = false;
	carbonBlockEnd = false;
}


SqlUtil::~SqlUtil()
{
}

SqlUtil::SqlUtil(const SqlUtil&) {

}

//在此处初始化
SqlUtil* SqlUtil::instance = new SqlUtil();
SqlUtil* SqlUtil::getInstance() {
	return instance;
}


void SqlUtil::saveDeviceTem(char* userid, char* devicename, char* image_coordinate) {
	char sql[350];
	sprintf(sql, "%s%s%s%s%s%s%s", "insert into sys_device_module_info(userid,devicename,image_tem,utime,ctime)value( '", userid, "','", devicename, "','", image_coordinate, "',NOW(),NOW())");
	int ret = mysql_query(con, (char*)sql);       //执行sql语句
	if (ret != 0) {
		printf("error:%s\n", mysql_error(con));
	}
}

BlockInfoDetailsMo* SqlUtil::getBlockInfoByNumber(char* number)
{
	MYSQL_RES *res;   //结果集
	char sql[300];
	sprintf(sql, "%s%s%s", "select * from sys_carbon_block_info where number =  '", number, "'");
	mysql_query(con, (char*)sql);       //执行sql语句
	res = mysql_store_result(con);      //将查询结果装进MYSQL_RES

	int rows = mysql_num_rows(res);      //获取结果行数
	printf("the number of the result is %d\n", rows);
	BlockInfoDetailsMo* blockInfoDetailsMo = NULL;
	while (rows--)
	{
		MYSQL_ROW row = mysql_fetch_row(res); //从结果集中获取一行
		blockInfoDetailsMo = new BlockInfoDetailsMo();
		blockInfoDetailsMo->id = atoi(row[0]);
		memcpy(blockInfoDetailsMo->defect_details, row[7], strlen(row[7]));
		blockInfoDetailsMo->photonum = atoi(row[4]);
		blockInfoDetailsMo->state = atoi(row[5]);
		blockInfoDetailsMo->type = atoi(row[6]);

	}
	mysql_free_result(res);   //查询完后记得要释放
							  //查看碳块对应的照片信息
	char sql_search[300];
	sprintf(sql_search, "%s%s%s", "select * from carbon_block_photo_info where carbon_block_id = '", number, "'");

	mysql_query(con, (char*)sql_search);       //执行sql语句
	res = mysql_store_result(con);      //将查询结果装进MYSQL_RES
	rows = mysql_num_rows(res);      //获取结果行数
	printf("the number of the result is %d\n", rows);

	while (rows--)
	{
		MYSQL_ROW row = mysql_fetch_row(res); //从结果集中获取一行
		ImageDetailsMo* imageDetails = new ImageDetailsMo();
		sprintf(imageDetails->carbon_block_id, "%s", row[1]);
		sprintf(imageDetails->photonum, "%s", row[2]);
		sprintf(imageDetails->cameranum, "%s", row[3]);
		sprintf(imageDetails->originalpath, "%s", row[4]);
		sprintf(imageDetails->originalinfo, "%s", row[5]);
		imageDetails->state = atoi(row[7]);
		sprintf(imageDetails->describe, "%s", row[8]);
		sprintf(imageDetails->ctime, "%s", row[10]);
		imageDetails->type = atoi(row[9]);
		blockInfoDetailsMo->imageDetailsList.emplace(std::move(imageDetails));
	}

	mysql_free_result(res);   //查询完后记得要释放
	return blockInfoDetailsMo;
}

BlockInfoDetailsMo* SqlUtil::getBlockInfo()
{
	MYSQL_RES *res;   //结果集
	char sql[100];
	sprintf(sql, "%s", "select * from sys_carbon_block_info  order by `ctime` DESC LIMIT 1 ");
	mysql_query(con, (char*)sql);       //执行sql语句
	res = mysql_store_result(con);      //将查询结果装进MYSQL_RES
	int rows = mysql_num_rows(res);      //获取结果行数
	printf("the number of the result is %d\n", rows);
	BlockInfoDetailsMo* blockInfoDetailsMo = NULL;
	while (rows--)
	{
		MYSQL_ROW row = mysql_fetch_row(res); //从结果集中获取一行
		blockInfoDetailsMo = new BlockInfoDetailsMo();
		blockInfoDetailsMo->id = atoi(row[0]);
		memcpy(blockInfoDetailsMo->prolineid, row[2], strlen(row[2]));
		memcpy(blockInfoDetailsMo->defect_details, row[7], strlen(row[7]));
		memcpy(blockInfoDetailsMo->number, row[3], strlen(row[3]));
		memcpy(blockInfoDetailsMo->ctime, row[9], strlen(row[9]));
		blockInfoDetailsMo->photonum = atoi(row[4]);
		blockInfoDetailsMo->state = atoi(row[5]);
		blockInfoDetailsMo->type = atoi(row[6]);
		blockInfoDetailsMo->spurtcode = atoi(row[12]);
	}

	mysql_free_result(res);   //查询完后记得要释放
	return blockInfoDetailsMo;
}

void SqlUtil::createBlockInfo(int userid, char* number, char* prolineid)
{
	char sql[350];
	sprintf(sql, "%s%d%s%s%s%s%s", "insert into sys_carbon_block_info(userid,number,prolineid,utime,ctime) value( ", userid, " , '", number, "' , '", prolineid, "' ,NOW(),NOW())");
	mysql_query(con, (char*)sql);       //执行sql语句
}

void SqlUtil::upBlockInfo(char* number, int photonum, int state, int type, char* prolineid)
{
	char sql[350];
	//参数需要修改
	sprintf(sql, "%s%d%s%d%s%d%s%s%s%s%s%s", "update sys_carbon_block_info SET photonum = photonum + ", photonum, ", state = ", state, ", type = ", type, ", prolineid = '", prolineid, "', utime = NOW()", " where number = '", number, "'");
	mysql_query(con, (char*)sql);       //执行sql语句
}

void SqlUtil::createBlockImage(char* carbon_block_id, char* photonum, char* cameranum, char* originalpath)
{
	char sql[350];
	sprintf(sql, "%s%s%s%s%s%s%s%s%s", "insert into carbon_block_photo_info(carbon_block_id,photonum,cameranum,originalpath,utime,ctime) value('", carbon_block_id, "','", photonum, "','", cameranum, "','", originalpath, "',NOW(),NOW())");
	int ret = mysql_query(con, (char*)sql);       //执行sql语句
	if (ret != 0) {
		printf("error:%s\n", mysql_error(con));
	}
}

void SqlUtil::upBlockImage(int id, char* originalinfo, int state, char* describe, int type)
{
	char sql[20480];
	sprintf(sql, "%s%s%s%s%d%s%s%s%d%s%d", "update carbon_block_photo_info set ", " originalinfo = '", originalinfo, "' , state = ", state, " ,  `describe` = '", describe, "' , type = ", type, " where id = ", id);
	mysql_query(con, (char*)sql);       //执行sql语句
}

bool SqlUtil::getspurtcode(const char* carbon_block_id)
{
	MYSQL_RES *res;   //结果集
	char sql[300];
	sprintf(sql, "%s%s%s", "select * from sys_carbon_block_info where  spurtcode = 0 AND state = 0 AND number =  '", carbon_block_id, "'");
	mysql_query(con, (char*)sql);       //执行sql语句
	res = mysql_store_result(con);      //将查询结果装进MYSQL_RES

	int rows = mysql_num_rows(res);      //获取结果行数
	printf("the number of the result is %d\n", rows);
	mysql_free_result(res);   //查询完后记得要释放
	if (rows > 0) {
		//检测12张图片是否已经检测完毕
		//if (checkBlockFinish(carbon_block_id))
		//{
		return true;
		//}

	}
	else {
		return false;
	}

}

bool SqlUtil::checkBlockFinish(const char* carbon_block_id)
{
	MYSQL_RES *res;   //结果集
	char sql[300];
	sprintf(sql, "%s%s%s", "select * from carbon_block_photo_info where   ( state = 0 or state = 1 ) AND carbon_block_id =  '", carbon_block_id, "' ");
	mysql_query(con, (char*)sql);       //执行sql语句
	res = mysql_store_result(con);      //将查询结果装进MYSQL_RES

	int rows = mysql_num_rows(res);      //获取结果行数
	printf("the number of the result is %d\n", rows);
	mysql_free_result(res);   //查询完后记得要释放
	if (rows >= 12) {
		//检测12张图片是否已经检测完毕
		return true;
	}
	else {
		return false;
	}
}

bool SqlUtil::setspurtcode(const char* carbon_block_id)
{
	char sql[200];
	sprintf(sql, "%s%s%s", "update sys_carbon_block_info set  spurtcode = 1   where number = '", carbon_block_id, "'");
	mysql_query(con, (char*)sql);       //执行sql语句
	return true;
}

vector<ImageDetailsMo*> SqlUtil::getUntreatedFiles()
{
	MYSQL_RES *res;   //结果集
	char sql[300];
	sprintf(sql, "%s", "select * from carbon_block_photo_info  where state = 2  LIMIT 6 ");
	//sprintf(sql, "%s", "select * from (select  * , group_concat(DISTINCT cameranum)  from carbon_block_photo_info where state = 2 group by `cameranum` order by  `ctime` desc ,`cameranum` asc LIMIT 6) AS a  order by  `cameranum` asc");
	mysql_query(con, (char*)sql);       //执行sql语句
	res = mysql_store_result(con);      //将查询结果装进MYSQL_RES
	int rows = mysql_num_rows(res);      //获取结果行数
	printf("the number of the result is %d\n", rows);
	vector<ImageDetailsMo*> files;
	while (rows--)
	{
		MYSQL_ROW row = mysql_fetch_row(res); //从结果集中获取一行
		ImageDetailsMo* imageDetailsMo = new ImageDetailsMo();
		imageDetailsMo->id = atoi(row[0]);
		memcpy(imageDetailsMo->carbon_block_id, row[1], strlen(row[1]));
		memcpy(imageDetailsMo->photonum, row[2], strlen(row[2]));
		memcpy(imageDetailsMo->originalpath, row[4], strlen(row[4]));
		memcpy(imageDetailsMo->cameranum, row[3], strlen(row[3]));
		files.push_back(imageDetailsMo);
	}
	mysql_free_result(res);   //查询完后记得要释放
	return files;
}

Times  SqlUtil::stamp_to_standard(int64_t stampTime)
{
	time_t tick = (time_t)stampTime;
	struct tm tm;
	char s[100];
	Times standard;

	tm = *localtime(&tick);
	strftime(s, sizeof(s), "%Y-%m-%d %H:%M:%S", &tm);
	printf("%d: %s\n", (int)tick, s);

	standard.Year = atoi(s);
	standard.Mon = atoi(s + 5);
	standard.Day = atoi(s + 8);
	standard.Hour = atoi(s + 11);
	standard.Min = atoi(s + 14);
	standard.Second = atoi(s + 17);

	return standard;
}

bool SqlUtil::setBlockReadState(const char* carbon_block_id)
{
	char sql[200];
	sprintf(sql, "%s%s%s", "update sys_carbon_block_info set  readstate = 1   where number = '", carbon_block_id, "'");
	mysql_query(con, (char*)sql);       //执行sql语句
	return true;
}

bool SqlUtil::setBlockReadStateAll()
{
	return true;
}

int SqlUtil::getSamedayBlockNum()
{
	MYSQL_RES *res;   //结果集
	char sql[200];
	sprintf(sql, "select COUNT(1) FROM sys_carbon_block_info WHERE  year(ctime)=year(now()) and month(ctime)=month(now()) and day(ctime)=day(now()) ");
	mysql_query(con, (char*)sql);       //执行sql语句
	res = mysql_store_result(con);      //将查询结果装进MYSQL_RES
	int rows = mysql_num_rows(res);      //获取结果行数
	printf("the number of the result is %d\n", rows);
	while (rows--)
	{
		MYSQL_ROW row = mysql_fetch_row(res); //从结果集中获取一行
		mysql_free_result(res);   //查询完后记得要释放
		return atoi(row[0]);
	}
	mysql_free_result(res);   //查询完后记得要释放
	return 0;
}

bool SqlUtil::createDirectory(const std::string folder) {

	return true;
}