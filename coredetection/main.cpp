#include <cstdio>
#include "OpenCVImage.h"
int main()
{
    printf("hello from coredetection!\n");
	
	OpenCVImage* openCVImage = OpenCVImage::getInstance();
	//启动图片存储线程
	openCVImage->createCameraThreadStart();
	//启动图片检测线程
	openCVImage->carbonBlockThreadStart();
	//启动与PLC通信线程
	openCVImage->clientPLCThreadStart();
    return 0;
}