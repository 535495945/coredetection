#pragma once
#include<queue>
struct BlockInfoMo
{
	//编号
	char number[32];
	//时间
	char time[32];
	//是否缺陷
	int state;
	//描述
	char details[200];
	//状态
};

struct ReportQueryMo
{
	//时间
	char time[32];
	//正常碳块
	int normalBlock;
	//异常碳块
	int abnormalBlock;
	//良率
	float goodyield;
};


struct ReportTypeQueryMo
{
	//缺陷类型 -- 一个碳块可能有多种缺陷,复合缺陷暂归为一类
	int fatherCrack;//父级裂纹类型
	int type;
	//碳块总数
	int blockNum;
	//正常碳块数
	int normalBlock;
	//异常碳块数
	int abnormalBlock;
	//当前缺陷数
	int abnormalNum;
	//缺陷在所有碳块快照中的而比例
	float abnormalBlockyield;
	//缺陷在所有缺陷快照中的而比例
	float abnormalyield;
};

struct StatisticalAnalysisMo

{
	//时间
	char time[32];
	//正常碳块
	int normalBlock;
	//异常碳块
	int abnormalBlock;

};

struct StatisticalTypeAnalysisMo

{
	//时间
	char time[32];
	//裂纹
	int crackle;
	//氧化
	int oxidation;
	//掉棱
	int prism;
	//掉角
	int dropangle;
};


struct searchReport

{
	char time[32];
	//正常碳块
	int state;
	int count;
	//状态
};


/*
图片信息
*/
struct ImageDetailsMo
{
	int id;
	//碳块编号
	char carbon_block_id[64];
	//照片编号
	char photonum[64];
	//相机编号
	char cameranum[64];
	//原始照片位置
	char originalpath[10240];
	//缺陷的位置坐标
	char originalinfo[10240];
	//缺陷标记照片为证
	char defectpath[10240];
	//是否有缺陷 1.正常 0.缺陷 2.未处理
	int state;
	//缺陷描述
	char describe[10240];
	//缺陷类型
	int type;
	//创建时间
	char ctime[32];
};

/**
碳块详细数据
**/
struct  BlockInfoDetailsMo
{

	int id;
	char number[32];
	char ctime[32];
	char username[32];
	char prolineid[32];
	int photonum;
	int type;
	int state;
	char defect_details[300]; //缺陷详情保留
	int spurtcode;
	std::queue<ImageDetailsMo*> imageDetailsList;
};

struct ImageData
{
	unsigned char* image;
	int Len;
	char pImagePath[256];//路径
	char pImageName[256];
	char pImageNum[256];
	char strDriName[256];	//设备编号
	unsigned int    nDevTimeStampHigh;  // 时间戳高32位
	unsigned int    nDevTimeStampLow;   // 时间戳低32位
};

typedef struct times
{
	int Year;
	int Mon;
	int Day;
	int Hour;
	int Min;
	int Second;
}Times;

//缺陷类型EXCLE模板数据结构
struct DefectTypesName
{
	CString fatherCrack;//父级裂纹类型
	CString subCrack;	//子级裂纹
};