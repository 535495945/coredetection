#pragma once
#include <pthread.h>
#include "SqlUtil.h"
#include "PLCClient.h"
#include <string>
#include "GrabCamera.h"

class OpenCVImage
{
public:
	static OpenCVImage* getInstance();
	OpenCVImage();
	~OpenCVImage();
	void AllThreadStop();
	//�������
	bool createCameraThreadStart();
	//����plcͨѶ
	bool clientPLCThreadStart();
	//����̼����
	bool carbonBlockThreadStart();
	
	static void *threadCameraFction(void *arg);
	static void *clientPLCThread(void *arg);
	static void *carbonBlockThread(void *arg);
private:
	static OpenCVImage* instance;
	
	bool threadStartState;
	bool carbonBlockThreadState;
	bool clientPLCThreadState;
	bool allThreadStop;

};

