#pragma once
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include "MvCameraControl.h"

#define CAMERA_NUM             2
class GrabCamera
{
public:
	GrabCamera();
	~GrabCamera();
public:
	void PressEnterToExit(void);
	bool PrintDeviceInfo(MV_CC_DEVICE_INFO* pstMVDevInfo);
	int clientGrab();
	static void* WorkThread(void* pUser);

};

