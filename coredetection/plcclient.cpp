#include "plcclient.h"

PLCClient::PLCClient(const char* Addr, int Port, int Id)
{
	address = Addr;
	port = Port;
	id = Id;
}

int PLCClient::Connect()
{
	int rlt = 0;



	//用于记录错误信息并输出  
	int iErrMsg;
	
	char recvline[MAXLINE], sendline[MAXLINE];
	//启动WinSock  
	socketfd = socket(AF_INET, SOCK_STREAM, 0);

	memset(&sockaddr, 0, sizeof(sockaddr));
	sockaddr.sin_family = AF_INET;
	sockaddr.sin_port = htons(10004);
	inet_pton(AF_INET, address, &sockaddr.sin_addr);
	if ((connect(socketfd, (struct sockaddr*)&sockaddr, sizeof(sockaddr))) < 0)
	{
		printf("connect error %s errno: %d\n", strerror(errno), errno);
		rlt = errno;
	}

	printf("send message to server\n");


	return rlt;
}

int PLCClient::SendMsg(const char* msg, int len)
{
	int rlt = 0;

	int iErrMsg = 0;

	//发送消息，指定sock发送消息  

	if ((send(socketfd, msg, strlen(msg), 0)) < 0)
	{
		printf("send mes error: %s errno : %d", strerror(errno), errno);

		rlt = 1;
		return rlt; (0);
	}

	return rlt;
}

int PLCClient::RecvMsg()
{
	char revData[1024];
	int rlt = 0;

	rlt = recv(socketfd, revData, MAXLINE, 0);
	if (rlt > 0)
	{

		//第 11 位 是需要的 结果
		for (int i = 0; i <= 10; i++)
		{
			char pTemImageName[256] = { 0 };
			//printf("%02X ", revData[i]);
			sprintf(pTemImageName,"%02X ", revData[i]);
		}
		char rev[2] = { 0 };
		sprintf(rev, "%02X ", revData[10]);
		return atoi(rev);//

	}
	return -1;

}

void PLCClient::Close()
{
	close(socketfd);
}

void PLCClient::Modbus_sender_single(int Ref, int addr, int value)  //写一个寄存器，使用功能码16，修改后就可以写多个  
{
	unsigned char Temp_buf[20];
	Temp_buf[0] = Ref;
	Temp_buf[1] = 0;
	Temp_buf[2] = 0;
	Temp_buf[3] = 0;
	Temp_buf[4] = 0;//从ID开始到最后的字节数  
	Temp_buf[5] = 9;
	Temp_buf[6] = id;//从机ID  
	Temp_buf[7] = 16;//命令代码  
	Temp_buf[8] = (addr - 1) / 256;//addr head //开始的地址  
	Temp_buf[9] = (addr - 1) % 256;
	Temp_buf[10] = 0;//number of addr   //地址的长度  
	Temp_buf[11] = 1;
	Temp_buf[12] = 2;//# of Bytes for values    //一共多少byte的值  
	Temp_buf[13] = value / 256;//values           //具体的值，这里我只改一个寄存器，就写一个值  
	Temp_buf[14] = value % 256;
	SendMsg((char*)Temp_buf, 15);   //将报文发出，15为报文长度，这里是固定的  
}