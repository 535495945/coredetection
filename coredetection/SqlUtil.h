#pragma once

#include<queue>
#include <mysql.h>
#include <time.h>
#include "stdio.h"
#include <string>
#include <string.h>
#include "BlockInfoMo.h"
#include <vector>

using namespace std;

class SqlUtil
{
public:
	static SqlUtil* getInstance();
public:
	SqlUtil();
	~SqlUtil();
	SqlUtil(const SqlUtil&);
	static SqlUtil* instance;
private:
	MYSQL * con;

public:
	//打开设备
	int OpenDevice();
	//遍历设备
	void getCameraDevices();
	//保持设备模板信息
	void saveDeviceTem(char* userid, char* devicename, char* image_coordinate);
	//创建碳块详细
	void createBlockInfo(int userid, char* number, char* prolineid);
	//更新碳块详细
	//碳块编号，照片数量，碳块状态，缺陷类型，生产线编号
	void upBlockInfo(char* number, int photonum, int state, int type, char* prolineid);
	//通过主键获取碳块信息
	BlockInfoDetailsMo* getBlockInfoByNumber(char* number);
	//获取最近一个碳块数据信息
	BlockInfoDetailsMo* getBlockInfo();
	//创建碳块照片详细
	void createBlockImage(char* carbon_block_id, char* photonum, char* cameranum, char* originalpath);
	//更新碳块照片详情
	//主件 ,缺陷的位置坐标，是否有缺陷，缺陷描述，缺陷类型
	void upBlockImage(int id, char* originalinfo, int state, char* describe, int type);
	//实时获取当前最新碳块编号
	string getBlockNum();
	//获取喷码标签
	bool getspurtcode(const char* carbon_block_id);
	//设置喷码标签
	bool setspurtcode(const char* carbon_block_id);
	//检测碳块是否处理完成
	bool checkBlockFinish(const char* carbon_block_id);
	//获取未处理的图片信息
	vector<ImageDetailsMo*> getUntreatedFiles();
	//查询当天碳块数量
	int getSamedayBlockNum();
	//时间戳
	Times stamp_to_standard(int64_t stampTime);
	//设置已读状态
	bool setBlockReadState(const char* carbon_block_id);
	//批量设置已读状态
	bool setBlockReadStateAll();
	//创建文件夹
	bool createDirectory(const string folder);

public:
	bool carbonBlockStart;	//碳块开始触发 初始值为true ，触发后为false
	bool carbonBlockEnd;		//碳块结束触发 初始值为false ,触发后为true;
};

